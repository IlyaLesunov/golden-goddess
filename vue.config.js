module.exports = {
  lintOnSave: false,
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/',

  // Пути для хостинга
  // publicPath: process.env.NODE_ENV === 'production'
  //   ? '/'
  //   : '/',

  transpileDependencies: [
    'vuetify'
  ]
}
